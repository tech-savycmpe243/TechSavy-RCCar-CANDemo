/*
 * wrapper.h
 *
 *  Created on: Mar 3, 2019
 *      Author: Akshata Kulkarni
 */

#ifndef WRAPPER_H_
#define WRAPPER_H_

#include "stdbool.h"
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


bool switch_function(int number);
void c_LED_On(int number);
void c_LED_Off(int number);

void setLED(uint8_t ledNum, bool on);
void setLCD_Display(char num);
void Clear_Display(void);
uint8_t switch_detect(void);

#ifdef __cplusplus
}
#endif


#endif /* WRAPPER_H_ */

/*
 * wrapper.cpp
 *
 *  Created on: Mar 3, 2019
 *      Author: Akshata Kulkarni
 */


#include <stdbool.h>
#include  "io.hpp"
#include "LED.hpp"
#include "wrapper.h"
#include <stdio.h>

void c_LED_On(int number)
{
    LE.on(1);
}
void c_LED_Off(int number)
{
    LE.off(1);
}

bool switch_function(int number)
{
    bool res = SW.getSwitch(1);
    return res;
}

void setLED(uint8_t ledNum, bool on)
{
    LE.set(ledNum, on);
}

void setLCD_Display(char num)
{
    LD.setRightDigit(num);
}

void Clear_Display()
{
    LD.clear();
}

uint8_t switch_detect()
{
    uint8_t switches = SW.getSwitchValues();
    return switches;
}



#include "unity.h"
#include "CAN_Communication.h"
#include <stdint.h>
#include <stdbool.h>
#include "Mockwrapper.h"
#include "Mockcan.h"
#include "c_period_callbacks.h"
#include <string.h>


void test_CAN_Init_w(void){

    CAN_init_ExpectAndReturn(can1, 100, 100, 100, NULL, NULL,true);
   CAN_bypass_filter_accept_all_msgs_Expect();
   CAN_reset_bus_Expect(can1);
   C_period_init();
}

void test_CAN_Recieve(void)
{
    CAN_rx_ExpectAndReturn(can1,NULL,0,1);
    CAN_rx_IgnoreArg_msg();
    //setLCD_Display_Expect(1);
    CAN_rx_ExpectAndReturn(can1,NULL,0,0);
    CAN_rx_IgnoreArg_msg();
    //setLED_Expect(2,0);
    switch_detect_ExpectAndReturn(1);
    CAN_tx_ExpectAndReturn(can1, NULL, 0,1);
    CAN_tx_IgnoreArg_msg();
    C_period_10Hz(1);

     CAN_rx_ExpectAndReturn(can1,NULL,0,1);
     CAN_rx_IgnoreArg_msg();
     CAN_rx_ExpectAndReturn(can1,NULL,0,0);
     CAN_rx_IgnoreArg_msg();
     //setLED_Expect(2,0);
     switch_detect_ExpectAndReturn(0);
     CAN_tx_ExpectAndReturn(can1, NULL, 0,1);
     CAN_tx_IgnoreArg_msg();
     C_period_10Hz(1);

}

void test_CANresetAfterBusOff(void)
{
    CAN_is_bus_off_ExpectAndReturn(can1,1);
    CAN_bypass_filter_accept_all_msgs_Expect();
    CAN_reset_bus_Expect(can1);
    C_period_1Hz(1);
}






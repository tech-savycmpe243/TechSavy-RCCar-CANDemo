/**
 * @file
 *
 * The purpose of this "C" callbacks is to provide the code to be able
 * to call pure C functions and unit-test it in C test framework
 */
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <can.h>
#include <stdio.h>
#include "c_period_callbacks.h"
#include "CAN_Communication.h"

_Bool C_period_init(void) {

    CAN_Init_w();
    return true;
}
_Bool C_period_reg_tlm(void) {
    return true;
}

void C_period_1Hz(uint32_t count) {
    (void) count;
    CANresetAfterBusOff();

}

void C_period_10Hz(uint32_t count) {
    (void) count;
    CAN_Recieve();
    CAN_Transmit();

}

void C_period_100Hz(uint32_t count) {
    (void) count;

}

void C_period_1000Hz(uint32_t count) {
    (void) count;
}


/*
 * can_rx.c
 *
 *  Created on: Mar 23, 2019
 *      Author: prash
 */


#include "can_rx.h"
#include "c_io.h"
#include "printf_lib.h"

extern const uint32_t               MOTOR_NODE__MIA_MS = 3000;
extern const MOTOR_NODE_t           MOTOR_NODE__MIA_MSG = { 22, 30 };

MOTOR_NODE_t motor_status_msg = { 0 };

const can_t can = can1;
const uint32_t baudRate = 100;
const uint16_t rxQueueSize = 15;
const uint16_t txQueueSize = 15;
const uint32_t timeout_ms = 0;

bool can_init(void)
{
    if (CAN_init(can, baudRate, rxQueueSize, txQueueSize, 0, 0))
    {
        u0_dbg_printf("Can INIT SUCCESS\n");
        CAN_bypass_filter_accept_all_msgs();
        CAN_reset_bus(can);
        return true;
    }
    else
    {
        return false;
    }
}

void can_rx(void)
{
    can_msg_t can_msg;
    static bool flag = false;


    // Empty all of the queued, and received messages within the last 10ms (100Hz callback frequency)
    while (CAN_rx(can1, &can_msg, 0))
    {
        // Form the message header from the metadata of the arriving message
        dbc_msg_hdr_t can_msg_hdr;
        can_msg_hdr.dlc = can_msg.frame_fields.data_len;
        can_msg_hdr.mid = can_msg.msg_id;
       // u0_dbg_printf("Hello \n");
        // Attempt to decode the message (brute force, but should use switch/case with MID)
        if(can_msg_hdr.mid == 105){
            flag = false;
            setLED(1, false);
            dbc_decode_MOTOR_NODE(&motor_status_msg, can_msg.data.bytes, &can_msg_hdr);
            //u0_dbg_printf("MOTOR_PWM = %f,MOTOR_KPH = %f \n",motor_status_msg.MOTOR_PWM,motor_status_msg.MOTOR_KPH);
            setLCD_Display(motor_status_msg.MOTOR_KPH);
        }

    }

    if(dbc_handle_mia_MOTOR_NODE(&motor_status_msg, 10)){
        setLCD_Display(motor_status_msg.MOTOR_KPH);
        flag = true;
        setLED(1, true);
        u0_dbg_printf("Flag true \n");
    }

    /*if (flag == true) {
        setLED(1, true);
    } else {
        setLED(1, false);
    }*/

}

bool can_turn_on_bus_if_bus_off(void)
{
    if (CAN_is_bus_off(can))
    {
        CAN_bypass_filter_accept_all_msgs();
        CAN_reset_bus(can);
        return true;
    }
    return false;
}


/*
 * can_rx.h
 *
 *  Created on: Mar 23, 2019
 *      Author: prash
 */

#ifndef CAN_RX_H_
#define CAN_RX_H_



#ifdef __cplusplus
extern "C" {
#endif

#include "can.h"
#include "_can_dbc/generated_can.h"

bool can_init(void);

void can_rx(void);

bool can_turn_on_bus_if_bus_off(void);


#ifdef __cplusplus
}
#endif





#endif /* CAN_RX_H_ */

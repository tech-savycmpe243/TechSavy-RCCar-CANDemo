/*
 * c_io.cpp
 *
 *  Created on: Mar 4, 2019
 *      Author: vidushi
 */
#include "c_io.h"
#include "io.hpp"

void setLED(uint8_t ledNum, bool on)
{
    LE.set(ledNum, on);
}

void setLCD_Display(char num)
{
    LD.setNumber(num);
}

void setLCD_LEFT(char num)
{
    LD.setLeftDigit(num);
}

void setLCD_Right(char num)
{
    LD.setRightDigit(num);
}

void Clear_Display()
{
    LD.clear();
}

int switch_detect()
{
    uint8_t switches = SW.getSwitchValues();

    enum {
            sw1 = (1 << 0),
            sw2 = (1 << 1),
            sw3 = (1 << 2),
            sw4 = (1 << 3),
        };


    return switches;
}

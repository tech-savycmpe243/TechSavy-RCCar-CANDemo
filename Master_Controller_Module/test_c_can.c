
#include "unity.h"
#include "c_can.h"
#include "generated_can.h"
#include "Mockcan.h"
#include "Mockc_io.h"


void setUp(void)
{

}
void tearDown(void)
{

}

void test_c_CAN_init__return_true(void)
{
    can_t can = can1;

    // setup expectation
    CAN_bypass_filter_accept_all_msgs_Expect();
    CAN_reset_bus_Expect(can);

    CAN_init_ExpectAndReturn(can1, 100, 100, 100, NULL, NULL, true);

    TEST_ASSERT_TRUE(c_CAN_init());
}

void test_c_CAN_init__return_false(void)
{
    // setup expectation
    CAN_init_ExpectAndReturn(can1, 100, 100, 100, NULL, NULL, false);
    TEST_ASSERT_FALSE(c_CAN_init());
}



void test_gps_node(void)
{
	can_msg_t msg_to_return;
    dbc_msg_hdr_t can_msg_hdr;
	can_msg_hdr.dlc = 0x00;
    can_msg_hdr.mid = 107;
    msg_to_return.data.bytes[0] = 0xAA;
	GPS_LOCATION_t gps_data = {0};
	gps_data.CURRENT_LAT = 123;
    CAN_rx_ExpectAndReturn(can1, NULL, 0, true);
    CAN_rx_IgnoreArg_msg();
   
    CAN_rx_ExpectAndReturn(can1, NULL, 0, false);
    CAN_rx_IgnoreArg_msg();
    // When the function is called, write
    // value of msg_to_return to ptr msg
    CAN_rx_ReturnThruPtr_msg(&msg_to_return);

    TEST_ASSERT_TRUE(c_CAN_rx());

	TEST_ASSERT_EQUAL(107,can_msg_hdr.mid);
	TEST_ASSERT_FALSE(dbc_decode_GPS_LOCATION(&gps_data, NULL, &can_msg_hdr));
	//setLCD_Display_Expect(gps_data.CURRENT_LAT);
	
}

void test_bridge_node(void)
{
	can_msg_t msg_to_return;
	dbc_msg_hdr_t can_msg_hdr;
	can_msg_hdr.dlc = 0x00;
    can_msg_hdr.mid = 103;
	msg_to_return.data.bytes[0] = 0xAA;
	BRIDGE_NODE_t bridge_data = {0};
	bridge_data.BRIDGE_START = 1;
	CAN_rx_ExpectAndReturn(can1, NULL, 0, true);
    CAN_rx_IgnoreArg_msg();
    
	CAN_rx_ExpectAndReturn(can1, NULL, 0, false);
    CAN_rx_IgnoreArg_msg();
    // When the function is called, write
    // value of msg_to_return to ptr msg
    CAN_rx_ReturnThruPtr_msg(&msg_to_return);

    TEST_ASSERT_TRUE(c_CAN_rx());

	TEST_ASSERT_EQUAL(103,can_msg_hdr.mid);
	TEST_ASSERT_FALSE(dbc_decode_BRIDGE_NODE(&bridge_data, NULL, &can_msg_hdr));
	//setLED_Expect(1,1);
	//setLCD_Display_Expect(bridge_data.BRIDGE_START);
	
}


void test_sensor_node(void)
{
    can_msg_t msg_to_return;
	dbc_msg_hdr_t can_msg_hdr;
	can_msg_hdr.dlc = 0x00;
    can_msg_hdr.mid = 104;
	msg_to_return.data.bytes[0] = 0xAA;
	SENSOR_NODE_t sensor_data = {0};
	sensor_data.SENSOR_FRONT = 12;
	CAN_rx_ExpectAndReturn(can1, NULL, 0, true);
    CAN_rx_IgnoreArg_msg();

	CAN_rx_ExpectAndReturn(can1, NULL, 0, false);
    CAN_rx_IgnoreArg_msg();
    // When the function is called, write
    // value of msg_to_return to ptr msg
    CAN_rx_ReturnThruPtr_msg(&msg_to_return);

    TEST_ASSERT_TRUE(c_CAN_rx());
	TEST_ASSERT_EQUAL(104,can_msg_hdr.mid);
	TEST_ASSERT_FALSE(dbc_decode_SENSOR_NODE(&sensor_data, NULL, &can_msg_hdr));
	//setLED_Expect(2,1);
	//setLCD_Display_Expect(sensor_data.SENSOR_FRONT);
}

void test_c_CAN_turn_on_bus_if_bus_off__return_true(void)
{
    //setup expectation
    can_t can = can1;
    CAN_bypass_filter_accept_all_msgs_Expect();
    CAN_reset_bus_Expect(can);

    CAN_is_bus_off_ExpectAndReturn(can, true);

    TEST_ASSERT_TRUE(c_CAN_turn_on_bus_if_bus_off());
}


/*
 * c_io.cpp
 *
 *  Created on: Mar 23, 2019
 *      Author: halak
 */
#include "c_io.h"
#include "io.hpp"

void setLED(uint8_t ledNum, bool on)
{
    LE.set(ledNum, on);
}


void setLCD_Display(char num)
{
    LD.setNumber(num);
}

void Clear_Display(void)
{
    LD.clear();
}


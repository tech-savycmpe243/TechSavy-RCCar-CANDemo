/*
 * c_can.c
 *
 *  Created on: Mar 3, 2019
 *      Author: Aakash
 */
#include <stdint.h>
#include <stdbool.h>
#include "c_code/c_can.h"
#include "_can_dbc/generated_can.h"
#include "printf_lib.h"
#include "c_io.h"

BRIDGE_NODE_t bridge_data = {0};
SENSOR_NODE_t sensor_data = {0};
GPS_LOCATION_t gps_data = {0};

const can_t can = can1;
const uint32_t baudRate = 100;
const uint16_t rxQueueSize = 100;
const uint16_t txQueueSize = 100;
const uint32_t timeout_ms = 0;

#define BRIDGE_NODE 103
#define SENSOR_NODE 104
#define MOTOR_NODE 105
#define GPS_LOCATION 107

extern const uint32_t                             BRIDGE_NODE__MIA_MS = 3000;
extern const BRIDGE_NODE_t                        BRIDGE_NODE__MIA_MSG = { 1 };
extern const uint32_t                             SENSOR_NODE__MIA_MS = 3000;
extern const SENSOR_NODE_t                        SENSOR_NODE__MIA_MSG = { 25,0,0 };
extern const uint32_t                             GPS_LOCATION__MIA_MS = 3000;
extern const GPS_LOCATION_t                       GPS_LOCATION__MIA_MSG = { 37.3686485, -121.9153289 };


bool c_CAN_init(void)
{
    if (CAN_init(can, baudRate, rxQueueSize, txQueueSize, 0, 0))
    {
        // u0_dbg_printf("Can INIT SUCCESS\n");
        CAN_bypass_filter_accept_all_msgs();
        CAN_reset_bus(can);
        return true;
    }
    else
    {
        return false;
    }
}

bool c_CAN_rx(void)
{
    bool flag = false;
    can_msg_t can_msg;


    while(CAN_rx(can1, &can_msg, 0))
    {
        // Form the message header from the metadata of the arriving message
        dbc_msg_hdr_t can_msg_hdr;
        can_msg_hdr.dlc = can_msg.frame_fields.data_len;
        can_msg_hdr.mid = can_msg.msg_id;
        switch(can_msg_hdr.mid){

            case BRIDGE_NODE:
            {
                dbc_decode_BRIDGE_NODE(&bridge_data, can_msg.data.bytes, &can_msg_hdr);
                // u0_dbg_printf("bridge start : %d\n",bridge_data.BRIDGE_START);
                setLED(1,false);
                break;
            }
            case SENSOR_NODE:
            {
                dbc_decode_SENSOR_NODE(&sensor_data, can_msg.data.bytes, &can_msg_hdr);
                // u0_dbg_printf("Lidar angle : %d\n",sensor_data.LIDAR_ANGLE);
                // u0_dbg_printf("Lidar range : %d\n",sensor_data.LIDAR_RANGE);
                // u0_dbg_printf("sensor front : %d\n",sensor_data.SENSOR_FRONT);
                setLED(2,false);
                setLCD_Display(sensor_data.SENSOR_FRONT);
                flag = true;

                break;
            }
            case GPS_LOCATION:
            {
                dbc_decode_GPS_LOCATION(&gps_data, can_msg.data.bytes, &can_msg_hdr);
                //  u0_dbg_printf("Latitude : %d\n",gps_data.CURRENT_LAT);
                //u0_dbg_printf("Longitude : %d\n",gps_data.CURRENT_LONG);
                setLED(4, false);
                break;
            }
            default: break;
        }
    }

    if(dbc_handle_mia_BRIDGE_NODE(&bridge_data, 100))
    {
        setLED(1, true);;
    }
    if(dbc_handle_mia_SENSOR_NODE(&sensor_data, 100))
    {
        u0_dbg_printf("hello\n");
        setLED(2, true);
    }
    if (dbc_handle_mia_GPS_LOCATION(&gps_data, 100))
    {
        setLED(4, true);
    }

    if (flag)
    {
        MOTOR_NODE_t motor_data = {0};
        dbc_msg_hdr_t msg_hdr_tx = {0};
        motor_data.MOTOR_KPH = (float)sensor_data.SENSOR_FRONT;
        msg_hdr_tx = dbc_encode_MOTOR_NODE(can_msg.data.bytes,&motor_data);
        can_msg.msg_id = msg_hdr_tx.mid;
        can_msg.frame_fields.data_len = msg_hdr_tx.dlc;
        //        u0_dbg_printf("SENDING DATA TO MOTOR: %f\n",motor_data.MOTOR_KPH);
        //        u0_dbg_printf("motor mid %d\n",can_msg.msg_id);
        CAN_tx(can1, &can_msg, 0);
    }

    return true;
}

bool c_CAN_turn_on_bus_if_bus_off(void)
{
    if (CAN_is_bus_off(can))
    {
        CAN_bypass_filter_accept_all_msgs();
        CAN_reset_bus(can);
        return true;
    }
    return false;
}



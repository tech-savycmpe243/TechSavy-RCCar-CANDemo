/*
 * test_c_can.c
 *
 *  Created on: Mar 4, 2019
 *      Author: Aakash
 */
#include "unity.h"
#include "c_can.h"
#include "Mockc_io.h"
#include "Mockcan.h"
#include "../_can_dbc/generated_can.h"

void setUp(void)
{

}
void tearDown(void)
{

}

void test_c_CAN_init__return_true(void)
{
    can_t can = can1;
    // setup expectation
    CAN_bypass_filter_accept_all_msgs_Expect();
    CAN_reset_bus_Expect(can);

    CAN_init_ExpectAndReturn(can, 100, 15, 15, NULL, NULL, true);

    TEST_ASSERT_TRUE(c_CAN_init());
}

void test_c_CAN_init__return_false(void)
{
    can_t can = can1;
    // setup expectation
    CAN_init_ExpectAndReturn(can, 100, 15, 15, NULL, NULL, false);

    TEST_ASSERT_FALSE(c_CAN_init());
}

void test_c_CAN_tx_true(void)
{
    SENSOR_NODE_t sensor_cmd = {0};
    uint8_t sensorData = 21;
    getLSDData_ExpectAndReturn(sensorData);
    sensor_cmd.SENSOR_FRONT = sensorData;
    sensor_cmd.LIDAR_ANGLE = 0;
    sensor_cmd.LIDAR_RANGE = 0;
    can_msg_t can_msg = {0};
    dbc_msg_hdr_t can_msg_hdr = dbc_encode_SENSOR_NODE(can_msg.data.bytes, &sensor_cmd);
    can_msg.msg_id = can_msg_hdr.mid;
    can_msg.frame_fields.data_len = can_msg_hdr.dlc;
    can_t can = can1;
    CAN_tx_ExpectAndReturn(can, &can_msg, 0, true);
    c_CAN_tx();
}

void test_c_CAN_tx_false(void)
{
    SENSOR_NODE_t sensor_cmd = {0};
    uint8_t sensorData = 21;
    getLSDData_ExpectAndReturn(sensorData);
    sensor_cmd.SENSOR_FRONT = sensorData;
    sensor_cmd.LIDAR_ANGLE = 0;
    sensor_cmd.LIDAR_RANGE = 0;
    can_msg_t can_msg = {0};
    dbc_msg_hdr_t can_msg_hdr = dbc_encode_SENSOR_NODE(can_msg.data.bytes, &sensor_cmd);
    can_msg.msg_id = can_msg_hdr.mid;
    can_msg.frame_fields.data_len = can_msg_hdr.dlc;
    can_t can = can1;
    CAN_tx_ExpectAndReturn(can, &can_msg, 0, false);
    c_CAN_tx();
}

void test_c_CAN_turn_on_bus_if_bus_off__return_true(void)
{
    //setup expectation
    can_t can = can1;
    CAN_reset_bus_Expect(can);

    CAN_is_bus_off_ExpectAndReturn(can, true);

    TEST_ASSERT_TRUE(c_CAN_turn_on_bus_if_bus_off());
}

void test_c_CAN_turn_on_bus_if_bus_off__return_false(void)
{
    //setup expectation
    can_t can = can1;
    CAN_is_bus_off_ExpectAndReturn(can, false);

    TEST_ASSERT_FALSE(c_CAN_turn_on_bus_if_bus_off());
}

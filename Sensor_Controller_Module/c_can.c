/*
 * c_can.c
 *
 *  Created on: Mar 3, 2019
 *      Author: Aakash
 */
#include "c_can.h"
#include "c_io.h"
#include "../_can_dbc/generated_can.h"
#include <stdio.h>

//// The MIA functions require that you define the:
////   - Time when the handle_mia() functions will replace the data with the MIA
////   - The MIA data itself (ie: DRIVE_LED__MIA_MS)
//const uint32_t            DRIVE_LED__MIA_MS = 3000;
//const DRIVE_LED_t         DRIVE_LED__MIA_MSG = { 0 };

const can_t can = can1;
const uint32_t baudRate = 100;
const uint16_t rxQueueSize = 15;
const uint16_t txQueueSize = 15;
const uint32_t timeout_ms = 0;

bool c_CAN_init(void)
{
    if (CAN_init(can, baudRate, rxQueueSize, txQueueSize, 0, 0))
    {
        CAN_bypass_filter_accept_all_msgs();
        CAN_reset_bus(can);
        return true;
    }
    else
    {
        return false;
    }
}

void c_CAN_rx(void)
{
//    puts("INside c_CAN_rx\n");
//    can_msg_t can_msg = {0};
//    DRIVE_LED_t drive_led_can_msg = {0};
//
//#ifdef UNIT_TESTING
//#define FOREVER for(bool __once = CAN_rx(can, &can_msg, timeout_ms); __once; __once = !__once)
//#else
//#define FOREVER while(CAN_rx(can, &can_msg, timeout_ms))
//#endif
//    FOREVER
//    {
//        puts("INside While\n");
//        dbc_msg_hdr_t can_msg_hdr;
//        can_msg_hdr.dlc = can_msg.frame_fields.data_len;
//        can_msg_hdr.mid = can_msg.msg_id;
//
//        //Decode the message
//        if(dbc_decode_DRIVE_LED(&drive_led_can_msg, can_msg.data.bytes, &can_msg_hdr))
//        {
//            puts("Inside decode\n");
//            if (drive_led_can_msg.DRIVE_LED_unsigned == 0xAA)
//            {
//                puts("LED on\n");
//                setLED(1, true);
//            }
//            else
//            {
//                puts("LED off\n");
//                setLED(1, false);
//            }
//        }
//    }
//    puts("out of while\n");
//    dbc_handle_mia_DRIVE_LED(&drive_led_can_msg, 10);
}

void c_CAN_tx(void)
{
    SENSOR_NODE_t sensor_cmd = {0};
    uint16_t sensorData = (uint16_t)getLSDData();
    sensor_cmd.SENSOR_FRONT = sensorData;
    sensor_cmd.LIDAR_ANGLE = 0;
    sensor_cmd.LIDAR_RANGE = 0;
    can_msg_t can_msg = {0};
    dbc_msg_hdr_t can_msg_hdr = dbc_encode_SENSOR_NODE(can_msg.data.bytes, &sensor_cmd);
    can_msg.msg_id = can_msg_hdr.mid;
    can_msg.frame_fields.data_len = can_msg_hdr.dlc;

    if(CAN_tx(can, &can_msg, timeout_ms) == 1) {
        puts("OK");
    } else {
        puts("Not OK");
    }
}

bool c_CAN_turn_on_bus_if_bus_off(void)
{
    if (CAN_is_bus_off(can))
    {
//        CAN_bypass_filter_accept_all_msgs();
        CAN_reset_bus(can);
        return true;
    }
    return false;
}



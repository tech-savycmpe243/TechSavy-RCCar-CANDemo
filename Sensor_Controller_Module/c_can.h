/*
 * c_can.h
 *
 *  Created on: Mar 3, 2019
 *      Author: Aakash
 */

#ifndef C_CAN_H_
#define C_CAN_H_

#include "can.h"

#ifdef __cplusplus
extern "C" {
#endif

bool c_CAN_init(void);

void c_CAN_rx(void);

void c_CAN_tx(void);

bool c_CAN_turn_on_bus_if_bus_off(void);

#ifdef __cplusplus
}
#endif


#endif /* C_CAN_H_ */
